#!/usr/bin/php
<?php

// global settings ... 
$host="localhost";
$user="crazytronner";
$pw="JediPower";
$db="ct_forum";

// Events to capture :
// [1] Received login from 192.168.2.102:32852 via socket *.*.*.*:4534.
// [1] CT~Voodoo entered the game.
// [1] CT~Voodoo plays for Team blue.
// [1] received logout from 1.
// [17] Killing user 17, ping 1.25.
// [6] Poll "Kick Tobi" has been accepted.
// [0] CT~Voodoo plays for Purple Nutters.
// [1] CT~Voodoo wants to play for Purple Nutters on the next respawn.

// First, let's build a table with all matchcode to perform on std input ...
// let's define the functions for each event and store them in a array with the requiered parameters :
// we're going to store  the following data :
// - 1 - the matchcode,
// - 2 - the function to execute if matchcode is found,
// - 3 - a message in case the matchcode is found,
// - 4 - a message if the function returns true
// - 5 - a message if the function returns false
//
// the function must have 1 parameter and return a boolean, like this :
// function player_connection($res)
// where $res is the array result as a return of the preg_match function

$parser_tab = array(	array("`^\[([1-9]+)\] Received login from ([^:]+):`", "p_player_login", "", "", ""),					// Player connection
					array("`^\[([1-9]+)\] (.+) wants to play for .+ on the next respawn.`", "p_player_join_req", "", "", ""),	// Player join game request
					array("`^\[([1-9]+)\] Poll .+ has been accepted\.`", "p_player_disconnect", "", "", ""),				// Player kicked by a poll
					array("`^\[([1-9]+)\] Killing user`", "p_player_disconnect", "", "", ""),							// Player killed cause he is banned
					array("`^\[([1-9]+)\] received logout from [1-9]+\.`", "p_player_disconnect", "", "", ""),			// Player left
					array("`^\[([1-9]+)\] .+ AUTOKICK (ON|OFF)`", "p_set_autokick", "", "", "")						// Set autokick on/off command
				    );

// what to do when a player connect to the server
function p_player_login($res) {
	global $players;	// table where to store players data
	
        $sql_res = mysql_query("SELECT * FROM cters_last_ip_v WHERE user_ip = '".$res[2]."';") or die ( mysql_error() );
	if (mysql_num_rows($sql_res)==0) {
		$players[$res[1]]=array(False,$res[2]);
		if ($autokick_on) {
			echo "CONSOLE_MESSAGE 0xff0000 Welcome to CT Private Server. Do not try to join a team or you will be kicked automatically.\n";
		} else {
			echo "CONSOLE_MESSAGE 0x00ffff Welcome to CT Private Serve. The server is currently open to everyone. Feel free to join the game !\n";
		}
	} else {
		$players[$res[1]]=array(True,$res[2],mysql_result($sql_res,0,"username"));
		echo "CONSOLE_MESSAGE 0x00ff00 Welcome home ".mysql_result($sql_res,0,"username").". Feel free to join the game !\n";
	}
}

// what to do when a player ask to join the game
function p_player_join_req($res) {
	global $players;

    	if ((!$players[$res[1]][0])&&$autokick_on) {
		echo "kick ".$res[1]."\n";
	}
}

// what to do when a player left/ is kicked/can't join ...
function p_player_disconnect($res) {
	global $players;

    	unset($players[$res[1]]);
}

// what to do when a autokick command is received
function p_set_autokick($res) {
	global $players;
	global $autokick_on;
	
	if ($players[$res[1]][0]) {
		$autokick_on = ($res[2]=="ON"?TRUE:($res[2]=="OFF"?FALSE:$autokick_on)); 
		echo "CONSOLE_MESSAGE 0x00ff00>AUTOKICK IS NOW ".(($autokick_on?"ON":"OFF"))."\n";
	}
}

// =============
// = main program ...  =
// =============

// connect to database
mysql_connect($host, $user, $pw);
mysql_select_db($db);

// init some global vars
$autokick_on = TRUE;

while (1)  {
	$line = rtrim(fgets(STDIN, 1024));
	// loop on parsing rules (table parser_tab)
	foreach ($parser_tab as $rule) {
		if (preg_match($parser_tab[0], $line, $res)) {
			echo $parser_tab[2];			// send the "before" message ...
			if ($parser_tab[1]($res)) {		// here we call the function to run if matchcode is found, and test the result to send messages if needed
				echo $parser_tab[3];		// send the "true" message ...
			} else {
				echo $parser_tab[4];		// send the "false" message ...
			}
		}
	}
    usleep(10);
    //echo " \n";
}

