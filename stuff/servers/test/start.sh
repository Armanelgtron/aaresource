#!/bin/bash

#Written by Vertrex.

tron="/usr/local/bin/armagetronad-ap-dedicated"
server="/home/armagetron/servers/"$1"/"
var=$server"var"
settings=$server"settings"
command=$server"command.txt"
console=$server"console.txt"

### Comment ###
# $1 is the folder name of server given from start.sh it is executed

while true; do
	$tron --vardir $var --userconfigdir $settings --input $command >> $console
	echo "!============================== SERVER RESTARTING IMMEDIATELY ==============================!" >> $console
	sleep 1
done
