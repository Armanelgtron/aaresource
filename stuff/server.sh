#!/bin/bash

#Mostly written by Vertrex, added to by Nelg.

#USER SET VARIABLES

loc="/home/armagetron/servers/"$1"/" #where are the servers located?

#in case the defaults don't work
command=$loc"command.txt"
console=$loc"console.txt"

#END USER SET VARIABLES

usage="Usage: $0 [server] [start|stop]"
case $2 in
	start)
		if [ -d $loc ]; then
			if ! screen -list | grep -q $1-svr; then
				rm -rf $command; touch $command
				echo "Starting "$1"."
				screen -dmS $1-svr $loc/start.sh $1
			else
				echo $1" appears to already be running. See screen -ls"
			fi
		else
			echo $1" doesn't appear to be a valid server. It must be a server in the directory specified"
		fi
	;;
	stop)
		if ! screen -list | grep -q $1-svr; then
			echo $1" doesn't appear to be running. See screen -ls"
		else
			echo "Stopping "$1"."
			echo "EXIT" >> $command
			sleep 1
			screen -S $1-svr -X quit 
		fi
	;;
	*)
		echo $usage;
	;;
esac
